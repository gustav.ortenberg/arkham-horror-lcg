# Investigators

## Amanda Sharpe - Gustav
- MT: 3
- PT: 1
- XP: 7+6+8+2+2+14

## Carson Sinclair - Fredrik
- MT: 3
- PT: 1
- XP: 7+6+8+2+2+14

## Sefina Rousseau - Vidar
- MT: 4
- PT: 
- XP: 7+6+8+2+2+14

## Tony Morgan - Erik
- Lead Investigator
- MT: 2
- PT: 2
- XP: 7+6+8+2+2+14

# Campaign Log
- You haven't seen the last of 
  - the Red Gloved Man
- The cell told the truth to Taylor
- The cell appreciated the architecture
- The cell knows desi's past
- The cell made a deal with desi
- Deis's in your debt
- You know the passphrase
- The cell knows Amaranth's real name
- The cell made a deal with Thorne
- Thorne is the bearer of the sable glass
- Amaranth has left the Coterie
- The Red Gloved Man is the bearer of The Last Blossom (Can be encountered at: Kabul, Quito, San Juan, Reijkavik 14-C)
- The cell meddeled with Abarran's affairs 

## Keys
- The Eye of the Ravens | Bearer: Carson
- The Mirroring Blade | Bearer: Carson
- The Last Blossom | Stolen
- The Wellspring of Fortune | Bearer: 

## Bag mod
- -1 Elder thing 
- +3 tablet
- +3 cultist

## Time Passed: 
- Triggers: 7, 10, 15, 20, 24, 35
- Theta @ 14
- 25

## Travel log
1. London (@2)
2. Stockholm (@3)
3. Moscow (@4)
4. Bermuda (If tuwiul masai is here, may return) (@6)
5. Ybor City (May return, time after theta + password) (@7)
6. Havanna (@8)
7. San Francisco (@11)
8. Achorage (@12)
9. Ybor City (@16)
10. Marakesh (@19)
11. Monte Carlo (@25)

### Locked Locations
- Tunguska 59-Z (Game ending)
