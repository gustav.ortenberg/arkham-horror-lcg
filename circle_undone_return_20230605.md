# Campaign tarot reading
- Temperance +
- Justice +
- Hermit -
- Hanged Man +
- Hierophant -
- Lovers -
- The chariot +
- Wheel of Fortune -

# Investigators
## Silas Marsh
- PT: 1
- MT: 1
- XP: 2+5+6+5+5+3+2+3

## Luke Robinson
- PT: 2
- MT: 2
- XP: 3+5+6+5+5+3+2+3

# Campaign Log
- You have accepted your fate
- Doom approaches 
- the witches' spell was cast
- the investigators escaped the spectral realm
- Josef disappeared into the mist
- the investigators are enemies of the lodge
- The hour is nigh
- the investigators survived the watchers embrace
- 3 Heretics were unleashed unto Arkham
- the investigators discovered how to open the puzzle box
- the investigators sided with the coven
- Anette Mason is possessed by evil
- the investigators asked anette for assistance
- the path winds before you | Tally: IIII
- Add 1 -5 token

## Mementos Discovered
- Mezmerizing Flute
- Gilman's Journal
- Keziah's Formulae
- Worn Crucifix
- Wisp of spectral mist